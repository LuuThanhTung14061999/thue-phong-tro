package com.ktpm02.quanliphongtro.ui.admin.them;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.room.ColumnInfo;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.data.model.PhongTro;
import com.ktpm02.quanliphongtro.data.roomDatabase.AppDatabase;
import com.ktpm02.quanliphongtro.databinding.ActivityThemPhongTroBinding;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import static com.ktpm02.quanliphongtro.common.Constants.GALLERY_REQUEST;

public class ThemPhongTroActivity extends AppCompatActivity {
    private ActivityThemPhongTroBinding binding;
    private String tenChuPhongTro;
    private String soDienthoai;
    private long giaPhongTro;
    private String diaChi;
    private String ngay;
    private long dienTich;
    private String mieuTa;
    private byte[] image;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_them_phong_tro);

        binding.btnSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
            }
        });

        binding.imageBackMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.btnThem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                tenChuPhongTro = binding.edtChuTro.getText().toString();
                soDienthoai = binding.edtSdt.getText().toString();
                if (!binding.edtGia.getText().toString().equals(""))
                    giaPhongTro = Long.parseLong(binding.edtGia.getText().toString());
                diaChi = binding.edtGia.getText().toString();
                ngay = binding.edtGia.getText().toString();
                if (!binding.edtGia.getText().toString().equals(""))
                    dienTich = Long.parseLong(binding.edtGia.getText().toString());
                mieuTa = binding.edtGia.getText().toString();
                if (bitmap != null) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    int nh = (int) ( bitmap.getHeight() * (512.0 / bitmap.getWidth()) );
                    Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                    scaled.compress(Bitmap.CompressFormat.PNG, 1, stream);
                    image = stream.toByteArray();
                }

                if (tenChuPhongTro.equals("") || soDienthoai.equals("") || giaPhongTro == 0
                        || diaChi.equals("") || ngay.equals("") || dienTich == 0
                        || mieuTa.equals("") || bitmap == null) {
                    Toast.makeText(ThemPhongTroActivity.this, "Bạn cần nhập đầy đủ thông tin", Toast.LENGTH_SHORT).show();
                    return;
                }
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        PhongTro phongTro = new PhongTro(tenChuPhongTro, soDienthoai, giaPhongTro, diaChi, ngay, dienTich, mieuTa, image);
                        AppDatabase.getInMemoryDatabase(ThemPhongTroActivity.this).phongTroDao().insertPhongTro(phongTro);
                    }
                });
                thread.start();
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                Toast.makeText(ThemPhongTroActivity.this, "Thêm Trọ Thành Công", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        binding.btnHuyBo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.edtChuTro.setText(null);
                binding.edtDiaChi.setText(null);
                binding.edtDienTich.setText(null);
                binding.edtGia.setText(null);
                binding.edtMieuTa.setText(null);
                binding.edtSdt.setText(null);
            }
        });

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
            switch (requestCode) {
                case GALLERY_REQUEST:
                    Uri selectedImage = data.getData();
                    try {
                        final InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                        bitmap = BitmapFactory.decodeStream(imageStream);
                        binding.imagePhongTro.setImageBitmap(bitmap);
                    } catch (IOException e) {

                    }
                    break;
            }
    }
}