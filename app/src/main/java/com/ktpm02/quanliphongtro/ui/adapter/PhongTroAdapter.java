package com.ktpm02.quanliphongtro.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ktpm02.quanliphongtro.data.model.PhongTro;
import com.ktpm02.quanliphongtro.databinding.ItemPhongTroTrangchuBinding;

import java.util.List;

public class PhongTroAdapter extends RecyclerView.Adapter<PhongTroAdapter.TrangChuHolder> {
    private List<PhongTro> data;
    private LayoutInflater mInflater;
    private TrangChuListener listener;

    public PhongTroAdapter(LayoutInflater mInflater) {
        this.mInflater = mInflater;
    }

    public void setListener(TrangChuListener listener) {
        this.listener = listener;
    }

    public List<PhongTro> getData() {
        return data;
    }

    public void setData(List<PhongTro> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public TrangChuHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemPhongTroTrangchuBinding binding = ItemPhongTroTrangchuBinding.inflate(mInflater, parent, false);
        return new TrangChuHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull TrangChuHolder holder, final int position) {
        holder.binding.setItemPhongTro(data.get(position));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPhongTroClicked(position);
            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                listener.onPhongTroLongClicked(position);
                return false;
            }
        });
    }

    @Override
    public int getItemCount() {
        return data == null ? 0 : data.size();
    }

    public class TrangChuHolder extends RecyclerView.ViewHolder {
        private ItemPhongTroTrangchuBinding binding;

        public TrangChuHolder(@NonNull ItemPhongTroTrangchuBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface TrangChuListener {
        void onPhongTroClicked(int position);
        void onPhongTroLongClicked(int position);
    }
}