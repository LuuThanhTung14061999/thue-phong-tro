package com.ktpm02.quanliphongtro.common;

public class Constants {

    public static final int REQUEST_CODE_SEARCH_TO_KHOANG_GIA=1;
    public static final int REQUEST_CODE_SEARCH_TO_DIEN_TICH=2;
    public static final int REQUEST_CODE_ADMIN_TO_SUA=3;
    public static final int GALLERY_REQUEST=5;
    public static final int REQUEST_CODE_ADMIN_TO_THEM=4;
    public static final String  GIA_ID="GIA";
    public static final String  DIEN_TICH_ID="DIEN_TICH";
}
