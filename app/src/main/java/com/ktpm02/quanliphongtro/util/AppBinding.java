package com.ktpm02.quanliphongtro.util;

import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.ktpm02.quanliphongtro.R;

public class AppBinding {
    @BindingAdapter("thumb")
    public static void thumb(ImageView im, byte[] image) {
        Glide.with(im)
                .load( BitmapFactory.decodeByteArray(image, 0, image.length))
                .error(R.drawable.ic_person_24)
                .into(im);
    }
}
