package com.ktpm02.quanliphongtro.ui.doimatkhau;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Toast;

import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.data.model.User;
import com.ktpm02.quanliphongtro.data.roomDatabase.AppDatabase;
import com.ktpm02.quanliphongtro.databinding.ActivityChangPassWordBinding;

public class ChangPassWordActivity extends AppCompatActivity {

    private ActivityChangPassWordBinding binding;

    private boolean isCheckShowOldPassword=false;
    private boolean isCheckShowNewPassword=false;
    private boolean isCheckShowConfirmNewPassword=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_chang_pass_word);

        binding.imOldPassWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isCheckShowOldPassword){
                    isCheckShowOldPassword=false;
                    binding.edtOldPassWord.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }else {
                    isCheckShowOldPassword=true;
                    binding.edtOldPassWord.setTransformationMethod(null);
                }
            }
        });

        binding.imNewPassWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isCheckShowNewPassword){
                    isCheckShowNewPassword=false;
                    binding.edtNewPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }else {
                    isCheckShowNewPassword=true;
                    binding.edtNewPassword.setTransformationMethod(null);
                }
            }
        });

        binding.imXacNhanPassWord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isCheckShowConfirmNewPassword){
                    isCheckShowNewPassword=false;
                    binding.edtAgainNewPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }else {
                    isCheckShowConfirmNewPassword=true;
                    binding.edtAgainNewPassword.setTransformationMethod(null);
                }
            }
        });

        binding.imageBackMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User user= (User) getIntent().getSerializableExtra("User");
                if(binding.edtOldPassWord.getText().toString().equals("")||binding.edtNewPassword.getText().toString().equals("")
                        ||binding.edtAgainNewPassword.getText().toString().equals("")){
                    Toast.makeText(ChangPassWordActivity.this, "Bạn cần nhập đầy đủ thông tin", Toast.LENGTH_SHORT).show();

                    return;
                }
                if(binding.edtOldPassWord.getText().toString().compareTo(user.getMatKhau())!=0){
                    binding.edtOldPassWord.setText("");
                    Toast.makeText(ChangPassWordActivity.this, "Bạn nhập sai mật khẩu cũ", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(binding.edtOldPassWord.getText().toString().compareTo(binding.edtNewPassword.getText().toString())==0){
                    binding.edtNewPassword.setText("");
                    binding.edtAgainNewPassword.setText("");
                    Toast.makeText(ChangPassWordActivity.this, "Mật khẩu mới không được giống mật khẩu cũ", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(binding.edtNewPassword.getText().toString().compareTo(binding.edtAgainNewPassword.getText().toString())!=0){
                    binding.edtNewPassword.setText("");
                    binding.edtAgainNewPassword.setText("");
                    Toast.makeText(ChangPassWordActivity.this, "Mật khẩu xác nhận khác với mật khẩu mới", Toast.LENGTH_SHORT).show();
                    return;
                }

                user.setMatKhau(binding.edtNewPassword.getText().toString());
                AppDatabase.getInMemoryDatabase(ChangPassWordActivity.this).userDao().updateUser(user);

                Toast.makeText(ChangPassWordActivity.this, "Bạn đã đổi mật khẩu thành công", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
}