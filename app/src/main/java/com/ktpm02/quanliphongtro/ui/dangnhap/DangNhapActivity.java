package com.ktpm02.quanliphongtro.ui.dangnhap;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.ui.admin.AdminActivity;
import com.ktpm02.quanliphongtro.ui.dangki.DangKiActivity;
import com.ktpm02.quanliphongtro.data.model.User;
import com.ktpm02.quanliphongtro.data.roomDatabase.AppDatabase;
import com.ktpm02.quanliphongtro.databinding.ActivityDangNhapBinding;
import com.ktpm02.quanliphongtro.ui.trangchu.TrangChuActivity;

import java.util.List;

public class DangNhapActivity extends AppCompatActivity implements View.OnClickListener {
    private final static int REQUEST_CODE_DANG_NHAP_SCREEN = 1;

    private SharedPreferences sharedPreferences;
    private ActivityDangNhapBinding binding;
    private String taiKhoan = null;
    private String matKhau = null;
    private int check = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dang_nhap);
        // Kiểm tra xem lần đăng nhập trước có ghi nhớ đăng nhập không, nếu có set lại text cho 2 ô nhập
        sharedPreferences = getSharedPreferences("LoginR", MODE_PRIVATE);
        check = sharedPreferences.getInt("Check", 0);
        if (check == 1) {
            binding.edtTaiKhoan.setText(sharedPreferences.getString("TaiKhoan", "ewe"));
            binding.edtMatKhau.setText(sharedPreferences.getString("MatKhau", "wew"));
            binding.checkboxPassword.setChecked(true);
        }else {
            Toast.makeText(this, "dfef"+check, Toast.LENGTH_SHORT).show();
        }
        //Thiết lập sự kiện xảy ra khi nhấn vào nút Đăng Kí
        binding.btnDangKiDangNhapScreen.setOnClickListener(this);

        //Thiết lập sự kiện xảy ra khi nhấn vào nút Đăng Nhập
        binding.btnDangNhap.setOnClickListener(this);

        eventCheckBox();
    }

    private void eventCheckBox() {
        binding.checkboxPassword.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @SuppressLint("CommitPrefEdits")
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    //sharedpreferences Là một cách lưu trữ dữ liệu nhỏ, đơn giản trong
                    // android dưới dạng file xml với những cặp key - value
                    //- Lưu trữ theo ứng dụng của người dùng cài đặt, nên khi gỡ ứng dụng dữ liệu sẽ mất theo.
                    //- Dùng khi lưu trữ tiến trình progressbar, hoặc là username, password.
                    // khi người dùng ấn vào nút ghi nhớ đăng nhập thì lần sau khi vào nó sẽ điền luôn.
                    //Khởi tạo kho lưu trữ data với name là login
                    //xác nhận đã hoàn thành việc lưu trữ
                    check=1;
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    taiKhoan=binding.edtTaiKhoan.getText().toString();
                    matKhau=binding.edtMatKhau.getText().toString();
                    editor.putString("TaiKhoan",taiKhoan);
                    editor.putString("MatKhau",matKhau);
                    editor.putInt("Check",1);
                    // Save.
                    editor.apply();
                }else {
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear();
                    editor.apply();
                }
            }
        });
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_dang_ki_dang_nhap_screen:
                Intent intent = new Intent(DangNhapActivity.this, DangKiActivity.class);
                startActivityForResult(intent, REQUEST_CODE_DANG_NHAP_SCREEN);
                break;
            case R.id.btn_dang_nhap:
                //Lấy Chuỗi text từ các ô nhập text
                taiKhoan = binding.edtTaiKhoan.getText().toString();
                matKhau = binding.edtMatKhau.getText().toString();

                // Kiểm tra xem các ô nhập text đã được nhập đầy đủ hay chưa, nếu chưa hiện ra thông báo
                if (taiKhoan.equals("") || matKhau.equals("")) {
                    Toast.makeText(DangNhapActivity.this, "Bạn cần nhập đầy đủ thông tin", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(taiKhoan.compareTo("admin")==0 && matKhau.compareTo("admin")==0){
                    Intent intent1=new Intent(DangNhapActivity.this,AdminActivity.class);
                    startActivity(intent1);
                    return;
                }

                // Có 3 trường hợp xảy ra:
                // 1: Sẽ kiểm tra xem Email đã tồn tại trong CSDL hay chưa
                // Nếu chưa yêu cầu nhập lại và reset text ở cả 2 ô email và tài khoản

                //2: Nếu Email đúng, mật khẩu sai thì sẽ yêu cầu nhập lại mật khẩu
                // và reset Text ở ô mật khẩu giữ nguyên ô email

                //3: nếu đúng cả 2 thì cho phép đăng nhập
                int check = 0;
                User user = null;
                List<User> userList = AppDatabase.getInMemoryDatabase(getApplicationContext()).userDao().getAll();
                for (int i = 0; i < userList.size(); i++) {
                    if (userList.get(i).getEmail().compareTo(taiKhoan) == 0 && userList.get(i).getMatKhau().compareTo(matKhau) != 0) {
                        check = 1;
                        break;
                    } else if (userList.get(i).getEmail().compareTo(taiKhoan) == 0
                            && userList.get(i).getMatKhau().compareTo(matKhau) == 0) {
                        user=userList.get(i);
                        check = 2;
                        break;
                    }
                }
                if (check == 1) {
                    Toast.makeText(DangNhapActivity.this
                            , "Thông tin mật khẩu bạn nhập chưa đúng,vui lòng nhập lại", Toast.LENGTH_SHORT).show();
                    binding.edtMatKhau.requestFocus();
                    binding.edtMatKhau.setText("");
                } else if (check == 2) {
                    //thiết lập sự kiện thay đổi của check box
                    Intent intent1 = new Intent(DangNhapActivity.this, TrangChuActivity.class);
                    intent1.putExtra("User",user);
                    startActivity(intent1);
                    finish();
                } else {
                    Toast.makeText(DangNhapActivity.this
                            , "Thông tin Email bạn nhập chưa đúng,vui lòng nhập lại", Toast.LENGTH_SHORT).show();
                    binding.edtTaiKhoan.setText("");
                    binding.edtTaiKhoan.requestFocus();
                    binding.edtMatKhau.setText("");
                }
        }

    }

    @Override
    public void onBackPressed() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAndRemoveTask();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_DANG_NHAP_SCREEN) {
            if (resultCode == RESULT_OK) {
                taiKhoan = data.getStringExtra("Email");
                matKhau = data.getStringExtra("MatKhau");
                binding.edtTaiKhoan.setText(taiKhoan);
                binding.edtMatKhau.setText(matKhau);
            }
        }
    }
}