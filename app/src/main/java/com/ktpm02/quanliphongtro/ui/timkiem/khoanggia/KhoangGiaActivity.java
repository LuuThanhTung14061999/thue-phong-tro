package com.ktpm02.quanliphongtro.ui.timkiem.khoanggia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.databinding.ActivityKhoangGiaBinding;

import static com.ktpm02.quanliphongtro.common.Constants.GIA_ID;

public class KhoangGiaActivity extends AppCompatActivity {

    private ActivityKhoangGiaBinding binding;
    private String gia = "0 -> 1 triệu";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_khoang_gia);

        eventRDGClicked();

        binding.btnApDung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra(GIA_ID, gia);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });

        binding.btnHuyBo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.rdb01Vnd.setChecked(true);
                Toast.makeText(KhoangGiaActivity.this, "Mời bạn chọn khoảng giá để tìm kiếm phòng trọ", Toast.LENGTH_SHORT).show();
            }
        });

        binding.imageBackMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @SuppressLint("NonConstantResourceId")
    private void eventRDGClicked() {
            binding.rdgKhoangGia.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rdb_0_1_vnd:
                        gia = binding.rdb01Vnd.getText().toString();
                        break;
                    case R.id.rdb_1_2_vnd:
                        gia = binding.rdb12Vnd.getText().toString();
                        break;
                    case R.id.rdb_2_5_vnd:
                        gia = binding.rdb25Vnd.getText().toString();
                        break;
                    case R.id.rdb_5_vnd:
                        gia = binding.rdb5Vnd.getText().toString();
                        break;
                }
            }
        });
    }
}