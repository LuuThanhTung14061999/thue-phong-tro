package com.ktpm02.quanliphongtro.ui.timkiem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.data.model.PhongTro;
import com.ktpm02.quanliphongtro.data.roomDatabase.AppDatabase;
import com.ktpm02.quanliphongtro.databinding.ActivityTimKiemBinding;
import com.ktpm02.quanliphongtro.ui.thongtinchitiet.ThongTinPhongTroActivity;
import com.ktpm02.quanliphongtro.ui.timkiem.dientich.DienTichActivity;
import com.ktpm02.quanliphongtro.ui.timkiem.khoanggia.KhoangGiaActivity;
import com.ktpm02.quanliphongtro.ui.adapter.PhongTroAdapter;
import java.util.LinkedList;
import java.util.List;

import static com.ktpm02.quanliphongtro.common.Constants.DIEN_TICH_ID;
import static com.ktpm02.quanliphongtro.common.Constants.GIA_ID;
import static com.ktpm02.quanliphongtro.common.Constants.REQUEST_CODE_SEARCH_TO_DIEN_TICH;
import static com.ktpm02.quanliphongtro.common.Constants.REQUEST_CODE_SEARCH_TO_KHOANG_GIA;

public class SearchActivity extends AppCompatActivity implements PhongTroAdapter.TrangChuListener {
    private ActivityTimKiemBinding binding;
    private PhongTroAdapter adapter;
    private List<PhongTro> dataPhongTro = new LinkedList<>();

    private String gia = null;
    private String dienTich = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_tim_kiem);
        binding.tvKhoangGia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchActivity.this, KhoangGiaActivity.class);
                startActivityForResult(intent, REQUEST_CODE_SEARCH_TO_KHOANG_GIA);
            }
        });

        binding.tvDienTich.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SearchActivity.this, DienTichActivity.class);
                startActivityForResult(intent, REQUEST_CODE_SEARCH_TO_DIEN_TICH);
            }
        });

        binding.btnTimKiem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initPhongTro();
                timKiemPhongTro();
            }
        });

        binding.imageBackMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void timKiemPhongTro() {
        if (gia == null || dienTich == null) {
            showDialogNullData("Mời bạn nhập dữ liệu để tìm kiếm");
            return;
        }
        if (gia.compareTo("0 -> 1 triệu") == 0) {
            showPhongTro(0,1000000);
        } else if (gia.compareTo("1 -> 2 triệu") == 0) {
            showPhongTro(1000000,2000000);
        } else if (gia.compareTo("2 -> 5 triệu") == 0) {
            showPhongTro(2000000,5000000);
        } else if (gia.compareTo("5 triệu trở lên") == 0) {
            showPhongTro(5000000);
        }
    }

    private void showPhongTro(int giaThap,int giaCao) {
        if (dienTich.compareTo("0 -> 20") == 0) {
            searchData(giaThap, giaCao, 0, 20);
        } else if (dienTich.compareTo("20 -> 40") == 0) {
            searchData(giaThap, giaCao, 20, 40);
        } else if (dienTich.compareTo("40 -> 60") == 0) {
            searchData(giaThap, giaCao, 40, 60);
        } else if (dienTich.compareTo("60 trở lên") == 0) {
            dataPhongTro.clear();
            dataPhongTro.addAll(AppDatabase.getInMemoryDatabase(getApplicationContext()).phongTroDao().getPhongTro(0, 1000000, 60));
            adapter.notifyDataSetChanged();
            if (dataPhongTro.size()<=0) {
                showDialogNullData("Không tìm thấy phòng trọ bạn yêu cầu");
            }
        }
    }

    private void showPhongTro(int gia) {
        if (dienTich.compareTo("0 -> 20") == 0) {
            searchData(gia, 0, 20);
        } else if (dienTich.compareTo("20 -> 40") == 0) {
            searchData(gia, 20, 40);
        } else if (dienTich.compareTo("40 -> 60") == 0) {
            searchData(gia, 40, 60);
        } else if (dienTich.compareTo("60 trở lên") == 0) {
            dataPhongTro.clear();
            dataPhongTro.addAll(AppDatabase.getInMemoryDatabase(getApplicationContext()).phongTroDao().getPhongTroFolowGia(gia, 60));
            adapter.notifyDataSetChanged();
            if (dataPhongTro.size()<=0) {
                showDialogNullData("Không tìm thấy phòng trọ bạn yêu cầu");
            }
        }
    }

    private void searchData(int giaThap, int giaCao, int dienTichThap, int DienTichCao) {
        dataPhongTro.clear();
        dataPhongTro.addAll(AppDatabase.getInMemoryDatabase(getApplicationContext()).phongTroDao().getPhongTro(giaThap, giaCao, dienTichThap, DienTichCao));
        adapter.notifyDataSetChanged();
        if (dataPhongTro.size()<=0) {
            showDialogNullData("Không tìm thấy phòng trọ bạn yêu cầu");
        }
    }

    private void searchData(int gia, int dienTichThap, int DienTichCao) {
        dataPhongTro.clear();
        dataPhongTro.addAll(AppDatabase.getInMemoryDatabase(getApplicationContext()).phongTroDao().getPhongTroFolowGia(gia, dienTichThap, DienTichCao));
        adapter.notifyDataSetChanged();
        if (dataPhongTro.size()<=0) {
            showDialogNullData("Không tìm thấy phòng trọ bạn yêu cầu");
        }
    }

    private void showDialogNullData(String query) {
        new AlertDialog.Builder(SearchActivity.this)
                .setTitle("Tìm Kiếm")
                .setMessage(query)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Continue with delete operation
                    }
                })
                .setNegativeButton(android.R.string.no, null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_SEARCH_TO_KHOANG_GIA:
                if (resultCode == RESULT_OK && data != null) {
                    gia = data.getStringExtra(GIA_ID);
                    binding.tvKhoangGia.setText("Từ " + gia + " (VNĐ)");
                }
                break;
            case REQUEST_CODE_SEARCH_TO_DIEN_TICH:
                if (resultCode == RESULT_OK && data != null) {
                    dienTich = data.getStringExtra(DIEN_TICH_ID);
                    binding.tvDienTich.setText("Từ " + dienTich + " (M2)");
                }
                break;
        }
    }

    private void initPhongTro() {
        adapter = new PhongTroAdapter(getLayoutInflater());
        binding.rcPhongTroSearch.setAdapter(adapter);
        adapter.setData(dataPhongTro);
        adapter.setListener(this);
    }

    @Override
    public void onPhongTroClicked(int position) {
        Intent intent=new Intent(SearchActivity.this, ThongTinPhongTroActivity.class);
        intent.putExtra("PhongTro",dataPhongTro.get(position));
        startActivity(intent);
    }

    @Override
    public void onPhongTroLongClicked(int position) {

    }
}