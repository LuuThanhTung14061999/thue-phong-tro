package com.ktpm02.quanliphongtro;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.ktpm02.quanliphongtro.ui.dangnhap.DangNhapActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Intent intent =new Intent(this, DangNhapActivity.class);
        startActivity(intent);
        finish();
    }
}