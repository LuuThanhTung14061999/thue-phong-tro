package com.ktpm02.quanliphongtro.ui.dangki;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.data.model.User;
import com.ktpm02.quanliphongtro.data.roomDatabase.AppDatabase;
import com.ktpm02.quanliphongtro.databinding.ActivityDangKiBinding;

import java.util.List;

public class DangKiActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityDangKiBinding binding;

    public DangKiActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dang_ki);
        binding.btnDangKi.setOnClickListener(this);
        binding.btnHuyBo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.edtHoTen.setText("");
                binding.edtTuoi.setText("");
                binding.edtCMND.setText("");
                binding.edtSDT.setText("");
                binding.edtDiaChi.setText("");
                binding.edtMatKhau.setText("");
                binding.edtNhapLaiMatKhau.setText("");
                binding.edtEmail.setText("");
            }
        });
        binding.imageBackMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        // Lấy ra các văn bản trong các ô nhập text và chuyển về dạng String
        String hoTen = binding.edtHoTen.getText().toString();
        String tuoi = binding.edtTuoi.getText().toString();
        String chungMinhNhanDan = binding.edtCMND.getText().toString();
        String soDT = binding.edtSDT.getText().toString();
        String diaChi = binding.edtDiaChi.getText().toString();
        String matKhau = binding.edtMatKhau.getText().toString();
        String matKhauNhapLai = binding.edtNhapLaiMatKhau.getText().toString();
        String email = binding.edtEmail.getText().toString();

        //Kiểm tra xem các ô đã được nhập hay chưa, nếu chưa hiện thông báo yêu cầu nhập lại
        if (hoTen.equals("") || tuoi.equals("") || chungMinhNhanDan.equals("") ||
                soDT.equals("") || email.equals("") ||
                diaChi.equals("") || matKhau.equals("") || binding.edtNhapLaiMatKhau.getText().toString().equals("")) {
            Toast.makeText(this, "Vui Lòng Nhập Đầy đủ Dữ Liệu", Toast.LENGTH_SHORT).show();
            return;
        }

        //Kiểm tra xem mật khẩu nhập lại và mật khẩu ban đầu có giống nhau không, nếu không yêu cầu nhập lại
        if (matKhau.compareTo(matKhauNhapLai) != 0) {
            Toast.makeText(DangKiActivity.this, "Mật khẩu bạn nhập không khớp, vui lòng nhập lại"
                    , Toast.LENGTH_SHORT).show();
            return;
        }

        //Kiểm tra xem Email đã được sử dụng hay chưa nếu chưa hiện ra thông báo
        for (int i = 0; i < AppDatabase.getInMemoryDatabase(getApplicationContext()).userDao().getAll().size(); i++) {
            if (AppDatabase.getInMemoryDatabase(getApplicationContext()).userDao().getAll().get(i).getEmail().toUpperCase().compareTo(email.toUpperCase()) == 0) {
                Toast.makeText(DangKiActivity.this
                        , "Email đã được sử dụng, vui lòng nhập tài khoản khác", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if(binding.edtEmail.getText().toString().compareTo("admin")==0&& binding.edtMatKhau.getText().toString().compareTo("admin")==0){
            Toast.makeText(DangKiActivity.this, "Tài khoản đã tồn tại"
                    , Toast.LENGTH_SHORT).show();
            return;
        }
        User user = new User(hoTen, tuoi, chungMinhNhanDan, soDT, diaChi, matKhau, email);
        AppDatabase.getInMemoryDatabase(getApplicationContext()).userDao().insertUser(user);
        Intent intent = new Intent();
        intent.putExtra("Email", email);
        intent.putExtra("MatKhau", matKhau);
        setResult(RESULT_OK, intent);
        Toast.makeText(this, "Đăng Kí Thành Công", Toast.LENGTH_SHORT).show();
        finish();
    }
}