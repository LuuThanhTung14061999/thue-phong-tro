package com.ktpm02.quanliphongtro.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.data.model.PhongTro;
import com.ktpm02.quanliphongtro.data.roomDatabase.AppDatabase;
import com.ktpm02.quanliphongtro.util.App;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class DataPhongTro {

    public void initDataPhongTro() {

        Bitmap bitmap = BitmapFactory.decodeResource(App.getContext().getResources(), R.drawable.a1);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        PhongTro phongTro1 = new PhongTro("Lưu Thanh Tung"
                , "0383338865", 100000, "Chi Đông - Mê Linh - Hà Nội"
                , "12/10/2020 15:30:45", 15
                , "Phòng trọ giá rẻ, tiện nghi thoáng mát",byteArray);
        if(AppDatabase.getInMemoryDatabase(App.getContext()).phongTroDao().countPhongTro()<=0){
            addPhongTroDatabase(phongTro1);
//            addPhongTroDatabase(phongTro2);
//            addPhongTroDatabase(phongTro3);
//            addPhongTroDatabase(phongTro4);
//            addPhongTroDatabase(phongTro5);
//            addPhongTroDatabase(phongTro6);
        }
    }

    private void addPhongTroDatabase(PhongTro phongTro) {
        AppDatabase.getInMemoryDatabase(App.getContext()).phongTroDao().insertPhongTro(phongTro);
    }
}
