package com.ktpm02.quanliphongtro.data.roomDatabase;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.ktpm02.quanliphongtro.data.model.PhongTro;
import com.ktpm02.quanliphongtro.data.model.User;

@Database(entities = {User.class, PhongTro.class}, version = 6,exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "DATABASE_NAME_102233";
    private static AppDatabase INSTANCE;
    public abstract UserDao userDao();
    public abstract PhongTroDao phongTroDao();
    public static AppDatabase getInMemoryDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class,DATABASE_NAME)
                    // Cho phép database chạy trên MainThread
                    // Khi gặp dự án thực tế không nên gọi lệnh này vì nó sẽ dễ bị khiến user block UI
                    // Tất cả những thao tác với CSDL nên được xử lí dưới backGround bằng asyn,thread,.. Khuyến khích dùng RXjava để quản lí các tác vụ ngầm
                    .allowMainThreadQueries()
                    // Lệnh này sẽ recreate lại database, Khi gặp dự án thực tế không nên dùng lệnh này vì nó sẽ xóa toàn bộ dữ liệu người dùng
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }

}
