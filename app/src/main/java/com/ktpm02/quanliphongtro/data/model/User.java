package com.ktpm02.quanliphongtro.data.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class User implements Serializable {
    @PrimaryKey(autoGenerate = true)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @ColumnInfo
    private String hoTen;
    @ColumnInfo
    private String tuoi;
    @ColumnInfo
    private String chungMinhNhanDan;
    @ColumnInfo
    private String soDT;
    @ColumnInfo
    private String diaChi;
    @ColumnInfo
    private String matKhau;
    @ColumnInfo
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public User(String hoTen, String tuoi, String chungMinhNhanDan, String soDT, String diaChi, String matKhau, String email) {
        this.hoTen = hoTen;
        this.tuoi = tuoi;
        this.chungMinhNhanDan = chungMinhNhanDan;
        this.soDT = soDT;
        this.diaChi = diaChi;
        this.matKhau = matKhau;
        this.email = email;
    }

    public String getMatKhau() {
        return matKhau;
    }

    public void setMatKhau(String matKhau) {
        this.matKhau = matKhau;
    }

    public String getHoTen() {
        return hoTen;
    }

    public void setHoTen(String hoTen) {
        this.hoTen = hoTen;
    }

    public String getTuoi() {
        return tuoi;
    }

    public void setTuoi(String tuoi) {
        this.tuoi = tuoi;
    }

    public String getChungMinhNhanDan() {
        return chungMinhNhanDan;
    }

    public void setChungMinhNhanDan(String chungMinhNhanDan) {
        this.chungMinhNhanDan = chungMinhNhanDan;
    }

    public String getSoDT() {
        return soDT;
    }

    public void setSoDT(String soDT) {
        this.soDT = soDT;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }
}
