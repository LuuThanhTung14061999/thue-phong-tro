package com.ktpm02.quanliphongtro.ui.trangchu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.data.DataPhongTro;
import com.ktpm02.quanliphongtro.data.model.PhongTro;
import com.ktpm02.quanliphongtro.data.model.User;
import com.ktpm02.quanliphongtro.data.roomDatabase.AppDatabase;
import com.ktpm02.quanliphongtro.databinding.ActivityTrangChuBinding;
import com.ktpm02.quanliphongtro.ui.adapter.PhongTroAdapter;
import com.ktpm02.quanliphongtro.ui.thongtincanhan.ThongTinCaNhanActivity;
import com.ktpm02.quanliphongtro.ui.thongtinchitiet.ThongTinPhongTroActivity;
import com.ktpm02.quanliphongtro.ui.timkiem.SearchActivity;

import java.util.LinkedList;
import java.util.List;

public class TrangChuActivity extends AppCompatActivity implements PhongTroAdapter.TrangChuListener {
    private ActivityTrangChuBinding binding;
    private PhongTroAdapter phongTroAdapter;
    private List<PhongTro> dataPhongTro = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_trang_chu);
        initPhongTro();
    }

    private void initPhongTro() {
        DataPhongTro dataPhongTro =new DataPhongTro();
        dataPhongTro.initDataPhongTro();
        this.dataPhongTro.addAll(AppDatabase.getInMemoryDatabase(TrangChuActivity.this).phongTroDao().getAll());
        phongTroAdapter = new PhongTroAdapter(getLayoutInflater());
        binding.rcPhongTro.setAdapter(phongTroAdapter);
        phongTroAdapter.setData(this.dataPhongTro);
        phongTroAdapter.setListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.personal:
                Intent intent = new Intent(this, ThongTinCaNhanActivity.class);
                intent.putExtra("USER",(User) getIntent().getSerializableExtra("User"));
                startActivity(intent);
                break;
            case R.id.search:
                Intent intent1 = new Intent(this, SearchActivity.class);
                startActivity(intent1);
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onPhongTroClicked(int position) {
        Intent intent=new Intent(TrangChuActivity.this, ThongTinPhongTroActivity.class);
        intent.putExtra("PhongTro",dataPhongTro.get(position));
        startActivity(intent);
    }

    @Override
    public void onPhongTroLongClicked(int position) {
    }
}