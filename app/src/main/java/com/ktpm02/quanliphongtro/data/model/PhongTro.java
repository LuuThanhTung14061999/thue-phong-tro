package com.ktpm02.quanliphongtro.data.model;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class PhongTro implements Serializable {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    private int id;
    @ColumnInfo
    private String tenChuPhongTro;
    @ColumnInfo
    private String soDienthoai;
    @ColumnInfo
    private long giaPhongTro;
    @ColumnInfo
    private String diaChi;
    @ColumnInfo
    private String ngay;
    @ColumnInfo
    private float dienTich;
    @ColumnInfo
    private String mieuTa;

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB)
    private byte[] image;

    public PhongTro(String tenChuPhongTro, String soDienthoai, long giaPhongTro, String diaChi, String ngay, float dienTich, String mieuTa, byte[] image) {
        this.tenChuPhongTro = tenChuPhongTro;
        this.soDienthoai = soDienthoai;
        this.giaPhongTro = giaPhongTro;
        this.diaChi = diaChi;
        this.ngay = ngay;
        this.dienTich = dienTich;
        this.mieuTa = mieuTa;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenChuPhongTro() {
        return tenChuPhongTro;
    }

    public void setTenChuPhongTro(String tenChuPhongTro) {
        this.tenChuPhongTro = tenChuPhongTro;
    }

    public String getSoDienthoai() {
        return soDienthoai;
    }

    public void setSoDienthoai(String soDienthoai) {
        this.soDienthoai = soDienthoai;
    }

    public long getGiaPhongTro() {
        return giaPhongTro;
    }

    public void setGiaPhongTro(long giaPhongTro) {
        this.giaPhongTro = giaPhongTro;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getNgay() {
        return ngay;
    }

    public void setNgay(String ngay) {
        this.ngay = ngay;
    }

    public float getDienTich() {
        return dienTich;
    }

    public void setDienTich(float dienTich) {
        this.dienTich = dienTich;
    }

    public String getMieuTa() {
        return mieuTa;
    }

    public void setMieuTa(String mieuTa) {
        this.mieuTa = mieuTa;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
