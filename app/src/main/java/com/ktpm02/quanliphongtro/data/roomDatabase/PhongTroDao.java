package com.ktpm02.quanliphongtro.data.roomDatabase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.ktpm02.quanliphongtro.data.model.PhongTro;
import java.util.List;

@Dao
public interface PhongTroDao {
    @Insert
    public void insertPhongTro(PhongTro... phongTros);

    @Update
    public void updatePhongTro(PhongTro... phongTros);

    @Delete
    public void deletePhongTro(PhongTro... phongTros);

    @Query("select * from PhongTro")
    public List<PhongTro> getAll();

    @Query("select * from PhongTro where id=:id")
    PhongTro getItem(int id);

    @Query("update PhongTro set soDienthoai=:soDienthoai,giaPhongTro=:giaPhongTro,tenChuPhongTro=:tenChuPhongTro1,diaChi=:diaChi,ngay=:ngay,mieuTa=:mieuTa,dienTich=:dienTich,image=:image where id=:position")
    void setPhongTro(String tenChuPhongTro1, String soDienthoai, long giaPhongTro, String diaChi, String ngay, long dienTich, String mieuTa, byte[] image, int position);

    @Query("select * from PhongTro where (giaPhongTro between :giaThap and :giaCao) and (dienTich between :dienTichThap and :dienTichCao) ")
    List<PhongTro> getPhongTro(long giaThap, long giaCao, long dienTichThap, long dienTichCao);

    @Query("select * from PhongTro where (giaPhongTro between :giaThap and :giaCao) and (dienTich >:dienTich) ")
    List<PhongTro> getPhongTro(long giaThap, long giaCao, long dienTich);

    @Query("select * from PhongTro where (giaPhongTro >:gia) and (dienTich between :dienTichThap and :dienTichCao) ")
    List<PhongTro> getPhongTroFolowGia(long gia, long dienTichThap, long dienTichCao);

    @Query("select * from PhongTro where (giaPhongTro >:gia) and (dienTich >:dienTich) ")
    List<PhongTro> getPhongTroFolowGia(long gia, long dienTich);

    @Query("select count(*) from PhongTro")
    int countPhongTro();
}
