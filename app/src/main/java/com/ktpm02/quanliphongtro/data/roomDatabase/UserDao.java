package com.ktpm02.quanliphongtro.data.roomDatabase;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.ktpm02.quanliphongtro.data.model.User;

import java.util.List;

@Dao
public interface UserDao {
    @Insert
    public void insertUser(User... users);

    @Update
    public void updateUser(User... users);

    @Delete
    public void deleteUser(User... users);

    @Query("select * from User")
    public List<User> getAll();

    @Query("select * from User where email=:email ")
    User getUser(String email);

    @Query("select count(*) from User")
    int countUser();
}
