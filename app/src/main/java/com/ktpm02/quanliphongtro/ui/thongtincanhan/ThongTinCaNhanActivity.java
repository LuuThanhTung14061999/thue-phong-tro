package com.ktpm02.quanliphongtro.ui.thongtincanhan;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;

import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.data.model.User;
import com.ktpm02.quanliphongtro.databinding.ActivityThongTinCaNhanBinding;
import com.ktpm02.quanliphongtro.ui.dangnhap.DangNhapActivity;
import com.ktpm02.quanliphongtro.ui.doimatkhau.ChangPassWordActivity;

public class ThongTinCaNhanActivity extends AppCompatActivity {
    private ActivityThongTinCaNhanBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_thong_tin_ca_nhan);
        User user= (User) getIntent().getSerializableExtra("USER");
        binding.tvEmail.setText(user.getEmail());
        binding.tvHoTen.setText(user.getHoTen());
        binding.tvTuoi.setText(user.getTuoi());
        binding.tvCmnd.setText(user.getChungMinhNhanDan());
        binding.tvPassword.setText(user.getMatKhau());

        binding.tvChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ThongTinCaNhanActivity.this, ChangPassWordActivity.class);
                intent.putExtra("User",user);
                startActivity(intent);
            }
        });

        binding.imageBackMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.btnDangXuat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ThongTinCaNhanActivity.this, DangNhapActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });
    }
}