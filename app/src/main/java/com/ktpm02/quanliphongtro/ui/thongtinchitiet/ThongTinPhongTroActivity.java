package com.ktpm02.quanliphongtro.ui.thongtinchitiet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.data.model.PhongTro;
import com.ktpm02.quanliphongtro.databinding.ActivityDetailPhongTroBinding;

public class ThongTinPhongTroActivity extends AppCompatActivity {

    private ActivityDetailPhongTroBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_detail_phong_tro);
        setData();
        binding.imageBackMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }

    private void setData() {
        PhongTro phongTro= (PhongTro) getIntent().getSerializableExtra("PhongTro");
        binding.tvDate.setText("Ngày đăng: "+phongTro.getNgay());
        binding.tvDiaChi.setText("Địa chỉ: "+phongTro.getDiaChi());
        binding.tvDienTich.setText("Diện tích: "+phongTro.getDienTich()+" m2");
        binding.tvSdt.setText("SĐT: "+phongTro.getSoDienthoai());
        binding.tvTenPhongTro.setText(phongTro.getTenChuPhongTro());
        binding.tvContentDetailPhongTro1.setText(phongTro.getMieuTa());
        binding.imPhongTro.setImageBitmap(BitmapFactory.decodeByteArray(phongTro.getImage(), 0, phongTro.getImage().length));
        binding.tvSdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" +phongTro.getSoDienthoai() ));
                startActivity(intent);
            }
        });
    }
}