package com.ktpm02.quanliphongtro.ui.admin.sua;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.data.model.PhongTro;
import com.ktpm02.quanliphongtro.data.roomDatabase.AppDatabase;
import com.ktpm02.quanliphongtro.databinding.ActivitySuaPhongTroBinding;
import com.ktpm02.quanliphongtro.ui.admin.them.ThemPhongTroActivity;
import com.ktpm02.quanliphongtro.util.App;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import static com.ktpm02.quanliphongtro.common.Constants.GALLERY_REQUEST;

public class SuaPhongTroActivity extends AppCompatActivity {

    private ActivitySuaPhongTroBinding binding;
    private String tenChuPhongTro;
    private String soDienthoai;
    private long giaPhongTro;
    private String diaChi;
    private String ngay;
    private long dienTich;
    private String mieuTa;
    private byte[] image;
    private Bitmap bitmap;
    private PhongTro phongTro;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_sua_phong_tro);

        position = getIntent().getIntExtra("PhongTro", 0);
        phongTro = AppDatabase.getInMemoryDatabase(SuaPhongTroActivity.this).phongTroDao().getItem(position);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                bitmap = BitmapFactory.decodeByteArray(phongTro.getImage(), 0, phongTro.getImage().length);
                binding.imagePhongTro.setImageBitmap(bitmap);
            }
        });
        thread.start();
        binding.edtChuTro.setText(phongTro.getTenChuPhongTro());
        binding.edtDiaChi.setText(phongTro.getDiaChi());
        binding.edtDienTich.setText(phongTro.getDienTich() + "");
        binding.edtGia.setText(phongTro.getGiaPhongTro() + "");
        binding.edtMieuTa.setText(phongTro.getMieuTa());
        binding.edtSdt.setText(phongTro.getSoDienthoai());

        binding.btnSelectImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, GALLERY_REQUEST);
            }
        });

        binding.imageBackMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.btnSua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tenChuPhongTro = binding.edtChuTro.getText().toString();
                soDienthoai = binding.edtSdt.getText().toString();
                if (!binding.edtGia.getText().toString().equals(""))
                    giaPhongTro = Long.parseLong(binding.edtGia.getText().toString());
                diaChi = binding.edtGia.getText().toString();
                ngay = binding.edtGia.getText().toString();
                if (!binding.edtGia.getText().toString().equals(""))
                    dienTich = Long.parseLong(binding.edtGia.getText().toString());
                mieuTa = binding.edtGia.getText().toString();
                if (bitmap != null) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    int nh = (int) (bitmap.getHeight() * (512.0 / bitmap.getWidth()));
                    Bitmap scaled = Bitmap.createScaledBitmap(bitmap, 512, nh, true);
                    scaled.compress(Bitmap.CompressFormat.PNG, 1, stream);
                    image = stream.toByteArray();
                }

                if (tenChuPhongTro.equals("") || soDienthoai.equals("") || giaPhongTro == 0
                        || diaChi.equals("") || ngay.equals("") || dienTich == 0
                        || mieuTa.equals("") || bitmap == null) {
                    Toast.makeText(SuaPhongTroActivity.this, "Bạn cần nhập đầy đủ thông tin", Toast.LENGTH_SHORT).show();
                    return;
                }
                AppDatabase.getInMemoryDatabase(SuaPhongTroActivity.this).phongTroDao().setPhongTro(tenChuPhongTro,soDienthoai,giaPhongTro,diaChi,ngay,dienTich,mieuTa,image,position);
                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                Toast.makeText(SuaPhongTroActivity.this, "Sửa Trọ Thành Công", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        binding.btnHuyBo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.edtChuTro.setText(null);
                binding.edtDiaChi.setText(null);
                binding.edtDienTich.setText(null);
                binding.edtGia.setText(null);
                binding.edtMieuTa.setText(null);
                binding.edtSdt.setText(null);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK)
            switch (requestCode) {
                case GALLERY_REQUEST:
                    Uri selectedImage = data.getData();
                    try {
                        final InputStream imageStream = getContentResolver().openInputStream(selectedImage);
                        bitmap = BitmapFactory.decodeStream(imageStream);
                        binding.imagePhongTro.setImageBitmap(bitmap);
                    } catch (IOException e) {

                    }
                    break;
            }
    }
}