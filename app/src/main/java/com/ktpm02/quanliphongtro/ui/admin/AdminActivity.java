package com.ktpm02.quanliphongtro.ui.admin;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.data.DataPhongTro;
import com.ktpm02.quanliphongtro.data.model.PhongTro;
import com.ktpm02.quanliphongtro.data.model.User;
import com.ktpm02.quanliphongtro.data.roomDatabase.AppDatabase;
import com.ktpm02.quanliphongtro.databinding.ActivityAdminBinding;
import com.ktpm02.quanliphongtro.ui.adapter.PhongTroAdapter;
import com.ktpm02.quanliphongtro.ui.admin.sua.SuaPhongTroActivity;
import com.ktpm02.quanliphongtro.ui.admin.them.ThemPhongTroActivity;
import com.ktpm02.quanliphongtro.ui.thongtincanhan.ThongTinCaNhanActivity;
import com.ktpm02.quanliphongtro.ui.timkiem.SearchActivity;
import com.ktpm02.quanliphongtro.ui.trangchu.TrangChuActivity;

import java.util.LinkedList;
import java.util.List;

import static com.ktpm02.quanliphongtro.common.Constants.REQUEST_CODE_ADMIN_TO_SUA;
import static com.ktpm02.quanliphongtro.common.Constants.REQUEST_CODE_ADMIN_TO_THEM;

public class AdminActivity extends AppCompatActivity implements PhongTroAdapter.TrangChuListener {

    private ActivityAdminBinding binding;
    private PhongTroAdapter phongTroAdapter;
    private List<PhongTro> dataPhongTro = new LinkedList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_admin);
        initPhongTro();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_admin_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.them:
                Intent intent = new Intent(this, ThemPhongTroActivity.class);
                startActivityForResult(intent,REQUEST_CODE_ADMIN_TO_THEM);
                break;
             }
        return super.onOptionsItemSelected(item);

    }

    private void initPhongTro() {
        DataPhongTro dataPhongTro =new DataPhongTro();
        dataPhongTro.initDataPhongTro();
        this.dataPhongTro.addAll(AppDatabase.getInMemoryDatabase(AdminActivity.this).phongTroDao().getAll());
        phongTroAdapter = new PhongTroAdapter(getLayoutInflater());
        binding.rcPhongTro.setAdapter(phongTroAdapter);
        phongTroAdapter.setData(this.dataPhongTro);
        phongTroAdapter.setListener(this);
    }

    @Override
    public void onPhongTroClicked(int position) {
        PopupMenu popup = new PopupMenu(this, binding.rcPhongTro.getChildAt(position));
        popup.inflate(R.menu.menu_popup_admin);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId()==R.id.sua){
                    Intent intent=new Intent(AdminActivity.this, SuaPhongTroActivity.class);
                    intent.putExtra("PhongTro",AppDatabase.getInMemoryDatabase(AdminActivity.this).phongTroDao().getAll().get(position).getId());
                    startActivityForResult(intent,REQUEST_CODE_ADMIN_TO_SUA);
                }
                return true;
            }
        });
        popup.show();
    }

    @Override
    public void onPhongTroLongClicked(int position) {
        new AlertDialog.Builder(AdminActivity.this)
                .setTitle("Xóa phòng trọ")
                .setMessage("Bạn có chắc chắn muốn xóa phòng trọ này không?")
                .setPositiveButton("Có", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        AppDatabase.getInMemoryDatabase(AdminActivity.this).phongTroDao().deletePhongTro(dataPhongTro.get(position));
                        dataPhongTro.remove(position);
                        phongTroAdapter.notifyDataSetChanged();
                        Toast.makeText(AdminActivity.this, "Đã xóa thành công", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Không", null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==REQUEST_CODE_ADMIN_TO_SUA){
            if(resultCode==RESULT_OK){
                dataPhongTro.clear();
                dataPhongTro.addAll(AppDatabase.getInMemoryDatabase(AdminActivity.this).phongTroDao().getAll());
                phongTroAdapter.notifyDataSetChanged();
            }
        }else if(requestCode==REQUEST_CODE_ADMIN_TO_THEM){
            if(resultCode==RESULT_OK){
                dataPhongTro.clear();
                dataPhongTro.addAll(AppDatabase.getInMemoryDatabase(AdminActivity.this).phongTroDao().getAll());
                phongTroAdapter.notifyDataSetChanged();
            }
        }
    }
}