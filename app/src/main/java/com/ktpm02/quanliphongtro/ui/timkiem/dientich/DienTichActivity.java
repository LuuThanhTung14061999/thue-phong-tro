package com.ktpm02.quanliphongtro.ui.timkiem.dientich;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.ktpm02.quanliphongtro.R;
import com.ktpm02.quanliphongtro.databinding.ActivityDienTichBinding;
import static com.ktpm02.quanliphongtro.common.Constants.DIEN_TICH_ID;

public class DienTichActivity extends AppCompatActivity {

    private ActivityDienTichBinding binding;
    private String dienTich = "0 -> 20";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_dien_tich);

        eventRDGClicked();

        binding.btnApDung.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra(DIEN_TICH_ID, dienTich);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });

        binding.btnHuyBo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.rdb020.setChecked(true);
                Toast.makeText(DienTichActivity.this, "Mời bạn chọn diện tích để tìm kiếm phòng trọ", Toast.LENGTH_SHORT).show();
            }
        });

        binding.imageBackMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    @SuppressLint("NonConstantResourceId")
    private void eventRDGClicked() {
        binding.rdgDienTich.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rdb_0_20:
                        dienTich = binding.rdb020.getText().toString();
                        break;
                    case R.id.rdb_20_40:
                        dienTich = binding.rdb2040.getText().toString();
                        break;
                    case R.id.rdb_40_60:
                        dienTich = binding.rdb4060.getText().toString();
                        break;
                    case R.id.rdb_60_tro_len:
                        dienTich = binding.rdb60TroLen.getText().toString();
                        break;
                }
            }
        });
    }
}